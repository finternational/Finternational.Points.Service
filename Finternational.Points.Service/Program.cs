using Finternational.Points.Service;
using Finternational.Points.Service.Configuration;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((context, services) =>
    {
        services.AddPersistenceLayer();
        services.AddProviders();
        services.AddGetters();
        services.AddHandlers();

        services.RegisterPersistenceSettings(context.Configuration);
        services.RegisterDataApiSettings(context.Configuration);

        services.AddHostedService<PointService>();
        services.AddHostedService<AutoSubService>();
    })
    .Build();

await host.RunAsync();
