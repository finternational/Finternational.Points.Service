﻿using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Fixture
{
    public class Score
    {
        [JsonPropertyName("halftime")]
        public Scores? Halftime { get; set; }
        [JsonPropertyName("fulltime")]
        public Scores? Fulltime { get; set; }
        [JsonPropertyName("extratime")]
        public Scores? Extratime { get; set; }
        [JsonPropertyName("penalty")]
        public Scores? Penalty { get; set; }
    }
}
