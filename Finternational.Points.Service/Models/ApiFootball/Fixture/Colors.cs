﻿
using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Fixture
{
    public class Colors
    {
        [JsonPropertyName("player")]
        public Colour? Player { get; set; }
        [JsonPropertyName("goalkeeper")]
        public Colour? Goalkeeper { get; set; }
    }
}
