﻿using Finternational.Points.Service.Providers;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Moq;
using Moq.AutoMock;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Finternational.Points.Service.Tests.Providers
{
    public class PointsProviderTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();
        private readonly Mock<IUnitOfWork> mockUnitOfWork;

        public PointsProviderTests()
        {
            var points = new List<EventPoints>()
            {
                new EventPoints(1, "0-60 minutes", 1, null),
                new EventPoints(2, "60+ minutes", 1, null),
                new EventPoints(3, "Goal", 6, 1),
                new EventPoints(4, "Goal", 6, 2),
                new EventPoints(5, "Goal", 5, 3),
                new EventPoints(6, "Goal", 4, 4),
                new EventPoints(7, "Assist", 3, null),
                new EventPoints(8, "Clean sheet", 4, 1),
                new EventPoints(9, "Clean sheet", 4, 2),
                new EventPoints(10, "Clean sheet", 1, 3),
                new EventPoints(11, "Saves", 1, 1),
                new EventPoints(12, "Penalty Save", 5, 1),
                new EventPoints(13, "Penalty Miss", -2, null),
                new EventPoints(14, "Conceded", -1, null),
                new EventPoints(16, "Yellow card", -1, null),
                new EventPoints(17, "Red Card", -3, null),
                new EventPoints(18, "Own goal", -2, null),
                new EventPoints(19, "Penalty Won", 3, null),
            };

            var mockPlayerRepo = this.autoMocker.GetMock<IPlayerRepository>();
            mockPlayerRepo
                .Setup(x => x.GetById(1))
                .ReturnsAsync(new Player(1, "John", "Goalkeeper", 1, new Country(1, ""), "", 1));
            mockPlayerRepo
                .Setup(x => x.GetById(2))
                .ReturnsAsync(new Player(2, "John", "Defender", 2, new Country(1, ""), "", 2));
            mockPlayerRepo
                .Setup(x => x.GetById(3))
                .ReturnsAsync(new Player(3, "John", "Midfielder", 3, new Country(1, ""), "", 3));
            mockPlayerRepo
                .Setup(x => x.GetById(4))
                .ReturnsAsync(new Player(4, "John", "Attacker", 4, new Country(1, ""), "", 4));

            var mockEventRepo = this.autoMocker.GetMock<IEventRepository>();
            mockEventRepo
                .Setup(x => x.GetEventPoints())
                .ReturnsAsync(points);

            mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockUnitOfWork
                .Setup(x => x.PlayerRepository)
                .Returns(mockPlayerRepo.Object);
            this.mockUnitOfWork
                .Setup(x => x.EventRepository)
                .Returns(mockEventRepo.Object);
        }

        [Theory]
        [InlineData(0, 6)]
        [InlineData(1, 1)]
        [InlineData(2, 8)]
        [InlineData(3, 7)]
        [InlineData(4, 2)]
        [InlineData(5, 7)]
        [InlineData(6, 6)]
        [InlineData(7, 3)]
        [InlineData(8, 7)]
        [InlineData(9, 6)]
        [InlineData(10, 7)]
        [InlineData(11, 12)]
        [InlineData(12, 9)]
        [InlineData(13, 13)]
        [InlineData(14, 0)]
        [InlineData(15, 2)]
        [InlineData(16, 0)]
        [InlineData(17, 0)]
        [InlineData(18, -2)]
        [InlineData(19, -2)]
        public async Task GetReturnsCorrectResults(int index, int expectedResult)
        {
            var performances = new List<Performance>()
            {
                new Performance(1, 1, 1, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 1), //GK 90MINS, CS        6
                new Performance(1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, false, 1), //GK 1MINS,          1
                new Performance(1, 1, 1, 90, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, false, 1), //GK, 90MINS, 1 GOAL, 8
                new Performance(1, 1, 1, 90, 3, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, false, 1), //GK, 90 MINS, 3 conceded, 1 goal, 7
                new Performance(1, 4, 1, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 1), //ATK 90 mins, cleansheet   2pts
                new Performance(1, 2, 1, 90, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, true, 1), //def 90, 1pw, 1pm, cs   7
                new Performance(1, 4, 1, 90, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, true, 1), //ATK 90 mins, cleansheet + goal   6pts
                new Performance(1, 3, 1, 90, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 1), //mid 90 mins, cleansheet, conceded 5  3pts
                new Performance(1, 1, 1, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, true, 1), //GK 90MINS, CS    3saves    7
                new Performance(1, 1, 1, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, true, 1), //GK 90MINS, CS    2saves    6
                new Performance(1, 1, 1, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, true, 1), //GK 90MINS, CS    4saves    7
                new Performance(1, 1, 1, 90, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 4, true, 1), //GK 90MINS, CS    4saves   pen saved 12
                new Performance(1, 4, 1, 90, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, true, 1), //ATK 90 mins, cleansheet + goal + assist  9pts
                new Performance(1, 3, 1, 90, 5, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, true, 1), //mid 90 mins, cleansheet, conceded 5, goals 2  13pts
                new Performance(1, 3, 1, 90, 5, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, true, 1), //mid 90 mins, cleansheet, conceded 5, 1 yellow, 1 red, 0pts
                new Performance(1, 3, 1, 90, 5, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, true, 1), //mid 90 mins, cleansheet, conceded 5, 1 yellow, 2pts
                new Performance(1, 3, 1, 90, 5, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, true, 1), //mid 90 mins, cleansheet, conceded 5, 1 red, 0pts
                new Performance(1, 3, 1, 90, 5, 0, 0, 0, 0, 0, 0, 2, 1, 0, 0, true, 1), //mid 90 mins, cleansheet, conceded 5, 2 yellow, 1 red, 0pts
                new Performance(1, 1, 1, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, false, 1), //GK 90MINS 2 OG  -2pts
                new Performance(1, 1, 4, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, false, 1), //ATTACK 90MINS 2 OG  -2pts
            };

            var sut = this.autoMocker.CreateInstance<PointsProvider>();

            var points = await sut.Get(performances[index], this.mockUnitOfWork.Object);

            Assert.Equal(expectedResult, points);
        }
    }
}
