﻿using Finternational.Points.Service.Exceptions;
using Finternational.Points.Service.Getter;
using Finternational.Points.Service.Models.ApiFootball.Fixture;
using Finternational.Points.Service.Providers;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Moq;
using Moq.AutoMock;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Finternational.Points.Service.Tests.Providers
{
    public class FixtureProviderTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetReturnsCorrectResults()
        {
            var mockGames = new List<Game>()
            {
                new Game(1, 1, "Test", 1, 1, "A", 1, new DateTime(), false, null, null, null, false),
                new Game(2, 1, "Test", 1, 1, "A", 1, new DateTime(), false, null, null, null, false),
            };

            var mockFixtureGetter = this.autoMocker.GetMock<IFixtureGetter>();

            mockFixtureGetter
                .Setup(x => x.Get(1))
                .ReturnsAsync(new FixtureResponse());
            mockFixtureGetter
               .Setup(x => x.Get(2))
               .ReturnsAsync(new FixtureResponse());

            var sut = this.autoMocker.CreateInstance<FixturesProvider>();
            var result = await sut.Get(mockGames, this.autoMocker.GetMock<IUnitOfWork>().Object);

            Assert.Equal(2, result.Count);
        } 

        [Fact]
        public async Task GetReturnsCorrectResultsWhenErrorIsThrow()
        {
            var mockGames = new List<Game>()
            {
                new Game(1, 1, "Test", 1, 1, "A", 1, new DateTime(), false, null, null, null, false),
                new Game(2, 1, "Test", 1, 1, "A", 1, new DateTime(), false, null, null, null, false),
            };

            var mockFixtureGetter = this.autoMocker.GetMock<IFixtureGetter>();

            mockFixtureGetter
                .Setup(x => x.Get(1))
                .ReturnsAsync(new FixtureResponse());
            mockFixtureGetter
               .Setup(x => x.Get(2))
               .ThrowsAsync(new ApiCallException("fail", 1, "test.com"));

            var mockRepo = this.autoMocker.GetMock<IApiCallFailRepository>();
            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(x => x.ApiCallFailRepository)
                .Returns(mockRepo.Object);

            var sut = this.autoMocker.CreateInstance<FixturesProvider>();
            var result = await sut.Get(mockGames, mockUnitOfWork.Object);

            mockUnitOfWork
                .Verify(x => x.ApiCallFailRepository.Insert(It.IsAny<ApiCallFail>()), Times.Once);
            Assert.Single(result);
        }
    }
}
