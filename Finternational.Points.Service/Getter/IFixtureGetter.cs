﻿using Finternational.Points.Service.Models.ApiFootball.Fixture;

namespace Finternational.Points.Service.Getter
{
    public interface IFixtureGetter
    {
        Task<FixtureResponse> Get(int fixtureId);
    }
}
