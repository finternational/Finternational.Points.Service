﻿namespace Finternational.Points.Service.Configuration
{
    public record DataApiSettings(string Url, string ApiKey, string ApiHost) { }
    public static class DataApiConfiguration
    {
        public static void RegisterDataApiSettings(this IServiceCollection services, IConfiguration config)
        {
            services.AddSingleton(new DataApiSettings(
                config.GetValue<string>("DataApi:Url"),
                config.GetValue<string>("DataApi:ApiKey"),
                config.GetValue<string>("DataApi:ApiHost")));
        }
    }
}
