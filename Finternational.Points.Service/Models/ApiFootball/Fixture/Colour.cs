﻿using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Fixture
{
    public class Colour
    {
        [JsonPropertyName("primary")]
        public string Primary { get; set; }
        [JsonPropertyName("number")]
        public string Number { get; set; }
        [JsonPropertyName("border")]
        public string Border { get; set; }
    }
}
