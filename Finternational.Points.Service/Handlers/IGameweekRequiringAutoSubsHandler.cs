﻿using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Handlers
{
    public interface IGameweekRequiringAutoSubsHandler
    {
        Task Handle(int gameweekId, IUnitOfWork unitOfWork);
    }
}
