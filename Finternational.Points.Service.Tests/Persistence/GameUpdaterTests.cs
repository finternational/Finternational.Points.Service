﻿using Finternational.Points.Service.Models.ApiFootball.Fixture;
using Finternational.Points.Service.Persistence;
using Finternational.Points.Service.Providers;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Moq;
using Moq.AutoMock;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Finternational.Points.Service.Tests.Persistence
{
    public class GameUpdaterTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        private async Task UpdateInPlayGamesDoesNotUpdateIfGameHasntChanged()
        {
            var stubFixtureResponse = new FixtureResponse() 
            { 
                Fixtures = new List<Fixture>() 
                { 
                    new Fixture 
                    { 
                        Goals = new Goals { Home = 2, Away = 1 },
                        Information = new FixtureInformation { Status = new Status { Long = "Match Unfinished" } }
                    }
                } 
            };

            var stubGame = new Game(1, 45434, "England", 2, 45555, "France", 1, new DateTime(2000, 5, 11), false, null, null, false, false);
            var stubGame2 = new Game(1, 45434, "England", 2, 45555, "France", 1, new DateTime(2000, 5, 11), false, null, null, false, false);

        
            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            var sut = this.autoMocker.CreateInstance<GameUpdater>();

            await sut.UpdateInPlayGame(stubFixtureResponse, stubGame, mockUnitOfWork.Object);

            mockUnitOfWork
                .Verify(x => x.GameRepository.Update(It.IsAny<Game>()), Times.Never);
        }

        [Fact]
        private async Task UpdateInPlayGamesUpdatesGameIfGamehasFinishedProvisionally()
        {
            var stubFixtureResponse = new FixtureResponse()
            {
                Fixtures = new List<Fixture>()
                {
                    new Fixture
                    {
                        Goals = new Goals { Home = 2, Away = 1 },
                        Information = new FixtureInformation { Status = new Status { Long = "Match Finished" } }
                    }
                }
            };

            var stubGame = new Game(1, 45434, "England", 2, 45555, "France", 1, new DateTime(2000, 5, 11), false, null, null, false, false);

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.GameRepository)
               .Returns(this.autoMocker.GetMock<IGameRepository>().Object);

            var sut = this.autoMocker.CreateInstance<GameUpdater>();

            await sut.UpdateInPlayGame(stubFixtureResponse, stubGame, mockUnitOfWork.Object);

            mockUnitOfWork
                .Verify(x => x.GameRepository.Update(It.IsAny<Game>()), Times.Once);
        }

        [Fact]
        public async Task UpdateProvisionallyFinishedGameSavesCorrectResults()
        {    
            var stubFixtureResponse = new FixtureResponse()
            {
                Fixtures = new List<Fixture>()
                {
                    new Fixture
                    {
                        Goals = new Goals { Home = 2, Away = 1 },
                        Information = new FixtureInformation { Status = new Status { Long = "Match Finished" } }
                    }
                }
            };

            var stubGame = new Game(1, 45434, "England", 2, 45555, "France", 1, new DateTime(2000, 5, 11), false, null, new DateTime(2000, 5, 12), true, false);
            Game updatedGame = stubGame;

            this.autoMocker.GetMock<IDateTimeProvider>()
                .Setup(x => x.Now())
                .Returns(new DateTime(2000, 5, 12, 4, 0, 0));

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();
            var mockGameRepo = this.autoMocker.GetMock<IGameRepository>();

            mockGameRepo
                .Setup(x => x.Update(It.IsAny<Game>()))
                .Callback<Game>(game => { updatedGame = game; });

            mockUnitOfWork.Setup(x => x.GameRepository)
               .Returns(mockGameRepo.Object);

            var sut = this.autoMocker.CreateInstance<GameUpdater>();

            await sut.UpdateProvisionallyFinishedGame(stubFixtureResponse, stubGame, mockUnitOfWork.Object);

            mockUnitOfWork
                .Verify(x => x.GameRepository.Update(It.IsAny<Game>()), Times.Once);

            Assert.True(updatedGame.Finished);
            Assert.Equal(updatedGame.NextCheck, new DateTime(2000, 5, 12, 16, 0, 0));
        }
    }
}
