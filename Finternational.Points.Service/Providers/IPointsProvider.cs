﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Providers
{
    public interface IPointsProvider
    {
        Task<int> Get(Performance performance, IUnitOfWork unitOfWork);
    }
}
