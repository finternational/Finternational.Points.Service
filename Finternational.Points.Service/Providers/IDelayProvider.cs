﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finternational.Points.Service.Providers
{
    public interface IDelayProvider
    {
        Task DelayMinutes(CancellationToken stoppingToken, int minutes);
    }
}
