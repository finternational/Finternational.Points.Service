﻿using Finternational.Points.Service.Configuration;
using Finternational.Points.Service.Exceptions;
using Finternational.Points.Service.Models.ApiFootball.Fixture;
using System.Net;
using System.Text.Json;

namespace Finternational.Points.Service.Getter
{
    public class FixtureGetter : IFixtureGetter
    {
        public DataApiSettings dataApiSettings;

        public FixtureGetter(DataApiSettings dataApiSettings)
        {
            this.dataApiSettings = dataApiSettings;
        }

        public async Task<FixtureResponse> Get(int fixtureId)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri($"{dataApiSettings.Url}/fixtures?id={fixtureId}");
                client.DefaultRequestHeaders.Add("x-rapidapi-host", dataApiSettings.ApiHost);
                client.DefaultRequestHeaders.Add("x-rapidapi-key", dataApiSettings.ApiKey);

                var response = await client.GetAsync(client.BaseAddress);

                if(response.StatusCode == HttpStatusCode.OK && response.Content != null)
                {
                    var jsonString = response.Content.ReadAsStringAsync();
                    var fixture =  JsonSerializer.Deserialize<FixtureResponse>(await jsonString);

                    GetErrors(fixture.Errors, client.BaseAddress.ToString());

                    if (fixture.Fixtures.Count == 0)
                    {
                        throw new ApiCallException("Fixtures count was zero", 200, client.BaseAddress.ToString());
                    }

                    return fixture;
                } 
                else
                {
                    throw new ApiCallException(response.Content == null ? "Content was null" : string.Empty, (int)response.StatusCode, client.BaseAddress.ToString());
                };             
            }
        }

        private void GetErrors(object errors, string request)
        {
            var errorsString = errors.ToString();

            if(errorsString != "[]")
            {
                throw new ApiCallException(errorsString, 200, request);
            }     
        }
    }
}
