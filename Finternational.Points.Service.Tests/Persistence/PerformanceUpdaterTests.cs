﻿using Finternational.Points.Service.Models.ApiFootball.Fixture;
using Finternational.Points.Service.Persistence;
using Finternational.Points.Service.Providers;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.AutoMock;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Finternational.Points.Service.Tests.Persistence
{
    public class PerformanceUpdaterTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task UpdatePerformancesHandlesIfStatisticsNotFound()
        {
            var stubGame = new Game(1, 132323, "France", 2, 35434, "Germany", 2, new DateTime(), false, null, null, null, false);
            var stubPerformances = new List<Performance>()
            {
                new Performance(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, true, 1)
            };

            var stubFixtureResponse = new FixtureResponse()
            {
                Fixtures = new List<Fixture>()
                {
                    new Fixture
                    {
                        Players = new List<PlayerResponse>()
                        {
                            new PlayerResponse
                            {
                                Players = new List<Models.ApiFootball.Fixture.Player>()
                                {
                                    new Models.ApiFootball.Fixture.Player { Details = new PlayerDetails { Id = 55 } }
                                }
                            },
                            new PlayerResponse
                            {
                                Players = new List<Models.ApiFootball.Fixture.Player>()
                                {
                                    new Models.ApiFootball.Fixture.Player { Details = new PlayerDetails { Id = 66 } }
                                }
                            }
                        }
                    }
                }
            };

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();
            var mockPerformanceRepo = this.autoMocker.GetMock<IPerformanceRepository>();

            mockPerformanceRepo
                .Setup(x => x.GetByGameId(1))
                .ReturnsAsync(stubPerformances);
            mockUnitOfWork
                .Setup(x => x.PerformanceRepository)
                .Returns(mockPerformanceRepo.Object);

            var sut = this.autoMocker.CreateInstance<PerformanceUpdater>();

            await sut.UpdatePerformances(stubFixtureResponse, stubGame, mockUnitOfWork.Object);

            this.autoMocker.GetMock<ILogger<PerformanceUpdater>>()
                .Verify(
                    logger => logger.Log(
                        It.Is<LogLevel>(logLevel => logLevel == LogLevel.Information),
                        It.Is<EventId>(eventId => eventId.Id == 0),
                        It.Is<It.IsAnyType>((@object, @type) => @object.ToString() == $"No statistics found for player {stubPerformances[0].Player}" && @type.Name == "FormattedLogValues"),
                        It.IsAny<Exception>(),
                        It.IsAny<Func<It.IsAnyType, Exception, string>>()),
                    Times.Once);

            mockUnitOfWork
                .Verify(x => x.PerformanceRepository.Update(It.IsAny<Performance>()), Times.Never);
        }

        [Fact]
        public async Task UpdatePerformancesSavesCorrectResults()
        {
            var updatedPerformance = new Performance(1, 55, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, true, 55);

            var stubGame = new Game(1, 132323, "France", 2, 35434, "Germany", 2, new DateTime(), false, null, null, null, false);
            var stubPerformances = new List<Performance>()
            {
                new Performance(1, 55, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, true, 55)
            };

            var expectedPerformance = new Performance(1, 55, 1, 90, 2, 200, 8, 12, 9, 10, 5, 0, 1, 2, 7, false, 55);
            var game = new Game(1, 1, "", 1, 1, "", 0, new DateTime(), false, null, null, null, false);
            this.autoMocker.GetMock<IConcededProvider>()
                .Setup(x => x.Get(55, It.IsAny<FixtureResponse>(), It.IsAny<Game>(), 58, It.IsAny<IUnitOfWork>()))
                .ReturnsAsync(2);

            var stubFixtureResponse = new FixtureResponse()
            {
                Fixtures = new List<Fixture>()
                {
                    new Fixture
                    {
                        Players = new List<PlayerResponse>()
                        {
                            new PlayerResponse
                            {
                                Players = new List<Models.ApiFootball.Fixture.Player>()
                                {
                                    new Models.ApiFootball.Fixture.Player
                                    {
                                        Details = new PlayerDetails { Id = 55 },
                                        Statistics = new List<Statistic>()
                                        {
                                            new Statistic()
                                            {
                                                Cards = new Cards() { Yellow = 0, Red = 1 },
                                                Games = new PlayerGame { Minutes = 58 },
                                                Goals = new PlayerGoals { Assists = 5, Conceded = 6, Saves = 7, Total = 8 },
                                                Penalty = new Penalty { Missed = 9, Saved = 10, Scored = 11, Won = 12 }                                    
                                            }
                                        }
                                    }
                                }
                            },
                            new PlayerResponse
                            {
                                Players = new List<Models.ApiFootball.Fixture.Player>()
                                {
                                    new Models.ApiFootball.Fixture.Player { Details = new PlayerDetails { Id = 66 } }
                                }
                            }
                        },
                        Events = new List<Models.ApiFootball.Fixture.Event>()
                        {
                            new Models.ApiFootball.Fixture.Event { Detail = "Own Goal", Player = new EventPlayer { Id = 55 } },
                            new Models.ApiFootball.Fixture.Event { Detail = "Own Goal", Player = new EventPlayer { Id = 55 } }
                        },
                        LineUps = new List<Root>()
                        {
                            new Root() { startXI = new List<Substitute>() { new Substitute { player = new LineUpPlayer { id = 55 } }  }, substitutes = new List<Substitute>() },
                            new Root() { startXI = new List<Substitute>(), substitutes = new List<Substitute>() }
                        },
                        Information = new FixtureInformation
                        {
                            Status = new Status { Elapsed = 90 }
                        }
                    }
                }
            };

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            this.autoMocker.GetMock<IPointsProvider>()
                .Setup(x => x.Get(It.IsAny<Performance>(), mockUnitOfWork.Object))
                .ReturnsAsync(200);

            var mockPerformanceRepo = this.autoMocker.GetMock<IPerformanceRepository>();

            mockPerformanceRepo
                .Setup(x => x.GetByGameId(1))
                .ReturnsAsync(stubPerformances);
            mockPerformanceRepo
                .Setup(x => x.Update(It.IsAny<Performance>()))
                .Callback<Performance>(performance => { updatedPerformance = performance; });

            mockUnitOfWork
                .Setup(x => x.PerformanceRepository)
                .Returns(mockPerformanceRepo.Object);

            var sut = this.autoMocker.CreateInstance<PerformanceUpdater>();

            await sut.UpdatePerformances(stubFixtureResponse, stubGame, mockUnitOfWork.Object);

            Assert.Equal(expectedPerformance, updatedPerformance);
        }
    }
}
