﻿using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Fixture
{
    public class Scores
    {
        [JsonPropertyName("home")]
        public int? Home { get; set; }
        [JsonPropertyName("away")]
        public int? Away { get; set; }
    }
}
