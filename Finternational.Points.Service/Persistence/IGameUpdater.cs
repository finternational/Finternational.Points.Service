﻿using Finternational.Points.Service.Models.ApiFootball.Fixture;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Persistence
{
    public interface IGameUpdater
    {
        Task UpdateInPlayGame(FixtureResponse fixture, Game game, IUnitOfWork unitOfWork);
        Task UpdateProvisionallyFinishedGame(FixtureResponse fixture, Game game, IUnitOfWork unitOfWork);
    }
}
