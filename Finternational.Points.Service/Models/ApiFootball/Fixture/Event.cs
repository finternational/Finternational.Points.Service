﻿using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Fixture
{
    public class Event
    {
        [JsonPropertyName("time")]
        public Time? Time { get; set; }
        [JsonPropertyName("team")]
        public Team? Team { get; set; }
        [JsonPropertyName("player")]
        public EventPlayer? Player { get; set; }
        [JsonPropertyName("assist")]
        public EventPlayer? Assist { get; set; }
        [JsonPropertyName("type")]
        public string Type { get; set; }
        [JsonPropertyName("detail")]
        public string Detail { get; set; }
        [JsonPropertyName("comments")]
        public string Comments { get; set; }
    }
}
