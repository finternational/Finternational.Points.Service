﻿using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Fixture
{
    public class PlayerResponse
    {
        [JsonPropertyName("team")]
        public Team? Team { get; set; }
        [JsonPropertyName("players")]
        public List<Player> Players { get; set; }

    }
}
