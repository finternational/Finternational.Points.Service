﻿using Finternational.Points.Service.Models.ApiFootball.Fixture;
using Finternational.Points.Service.Providers;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Persistence
{
    public class PerformanceUpdater : IPerformanceUpdater
    {
        private readonly IPointsProvider pointsProvider;
        private readonly IConcededProvider concededProvider;
        private readonly ILogger<PerformanceUpdater> logger;

        public PerformanceUpdater(IPointsProvider pointsProvider, IConcededProvider concededProvider, ILogger<PerformanceUpdater> logger)
        {
            this.concededProvider = concededProvider;
            this.pointsProvider = pointsProvider;
            this.logger = logger;
        }

        public async Task UpdatePerformances(FixtureResponse fixture, Game game, IUnitOfWork unitOfWork)
        {

            //minutes - work out when they were subbed on / started
            /*  get 'startMinute */
            /* work out if they were subbed on */



            var performances = await unitOfWork.PerformanceRepository.GetByGameId(game.Id);
            var joinedPlayers = fixture.Fixtures[0].Players[0].Players.Concat(fixture.Fixtures[0].Players[1].Players);

            //what about players who dont have a performance (and probably dont have a Player)

            foreach (var performance in performances)
            {
                var playerStats = joinedPlayers.Where(x => x?.Details?.Id == performance.PlayerExternalId).FirstOrDefault()?.Statistics;

                if (playerStats == null)
                {
                    logger.LogInformation($"No statistics found for player {performance.Player}");
                }
                else
                {
                    var ownGoals = fixture.Fixtures[0].Events.Where(x => x.Player?.Id == performance.PlayerExternalId && x.Detail == "Own Goal").Count();
                    var conceded = await this.concededProvider.Get((int)performance.PlayerExternalId, fixture, game, playerStats[0]?.Games?.Minutes ?? 0, unitOfWork);


                    var lineups = fixture.Fixtures[0].LineUps[0].startXI.Concat(fixture.Fixtures[0].LineUps[1].startXI).ToList();

                    var sub = true;
                    var startMinute = (int?)0;

                    if (performance.PlayerExternalId == 2098)
                    {
                        var p = "f";
                    }

                    if (lineups.Where(x => x.player.id == performance.PlayerExternalId).FirstOrDefault() != null)
                    {
                        sub = false;
                    }


                    var firstSub = fixture.Fixtures[0].Events.Where(x => x.Type == "subst").FirstOrDefault();

                    if (firstSub != null)
                    {
                        var id = firstSub.Player.Id;

                        if ((lineups.Where(x => x.player.id == id).FirstOrDefault() == null))
                        {
                            game = game with { TransfersReversed = true };
                        }
                    }

                    //if sub, see if they were subbed on
                    if (sub)
                    {
                        startMinute = null;
                        for(int i = 0; i < fixture.Fixtures[0].Events?.Count; i++)
                        {
                            if (game.TransfersReversed)
                            {
                                if (fixture.Fixtures[0].Events[i].Type == "subst" && fixture.Fixtures[0].Events[i].Player.Id == performance.PlayerExternalId)
                                {
                                    startMinute = (int)fixture.Fixtures[0].Events[i].Time.Elapsed;
                                }
                            }
                            else
                            {
                                if (fixture.Fixtures[0].Events[i].Type == "subst" && fixture.Fixtures[0].Events[i].Assist.Id == performance.PlayerExternalId)
                                {
                                    startMinute = (int)fixture.Fixtures[0].Events[i].Time.Elapsed;
                                }
                            }
                        }  
                    }


                    var endMinute = fixture.Fixtures[0].Information.Status.Elapsed.Value;
                    //this is unused

                    //then see if they were subbed off
                    for (int i = 0; i < fixture.Fixtures[0].Events?.Count; i++)
                    {
                        if (game.TransfersReversed)
                        {
                            if (fixture.Fixtures[0].Events[i].Type == "subst" && fixture.Fixtures[0].Events[i].Assist.Id == performance.PlayerExternalId)
                            {
                                endMinute = (int)fixture.Fixtures[0].Events[i].Time.Elapsed;
                            }
                        } 
                        else
                        {
                            if (fixture.Fixtures[0].Events[i].Type == "subst" && fixture.Fixtures[0].Events[i].Player.Id == performance.PlayerExternalId)
                            {
                                endMinute = (int)fixture.Fixtures[0].Events[i].Time.Elapsed;
                            }
                        }
                        
                    }

                    var minsPlayed = 0;

                    if(startMinute != null)
                    {
                        minsPlayed = endMinute - (int)startMinute;
                    }
                   



                    var updatedPerformance = performance with
                    {
                        Minutes = minsPlayed,
                        Conceded = conceded,
                        Goals = playerStats[0]?.Goals?.Total ?? 0,
                        PenaltyWon = playerStats[0]?.Penalty?.Won ?? 0,
                        PenaltyMissed = playerStats[0]?.Penalty?.Missed ?? 0,
                        PenaltySaved = playerStats[0]?.Penalty?.Saved ?? 0,
                        Assists = playerStats[0]?.Goals?.Assists ?? 0,
                        YellowCard = playerStats[0]?.Cards?.Yellow ?? 0,
                        RedCard = playerStats[0]?.Cards?.Red ?? 0,
                        Saves = playerStats[0]?.Goals?.Saves ?? 0,
                        OwnGoal = ownGoals,
                        CleanSheet = CleanSheet(conceded, playerStats[0]?.Games?.Minutes ?? 0)
                    };

                    var points = await this.pointsProvider.Get(updatedPerformance, unitOfWork);
                    var updatedPerformanceWithPoints = updatedPerformance with
                    {
                        Points = points
                    };

                    await unitOfWork.PerformanceRepository.Update(updatedPerformanceWithPoints);
                }
            }
        }

        private bool CleanSheet(int conceded, int minutes)
        {
            if (minutes >= 60 && conceded == 0)
            {
                return true;
            }

            return false;
        }
    }
}
