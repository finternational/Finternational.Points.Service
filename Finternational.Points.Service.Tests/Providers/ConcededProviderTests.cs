﻿using Finternational.Points.Service.Models.ApiFootball.Fixture;
using Finternational.Points.Service.Providers;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Moq;
using Moq.AutoMock;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Event = Finternational.Points.Service.Models.ApiFootball.Fixture.Event;
using Player = finternational_common.Entities.Player;
using Team = Finternational.Points.Service.Models.ApiFootball.Fixture.Team;

namespace Finternational.Points.Service.Tests.Providers
{
    public class ConcededProviderTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetReturnsCorrectResultIfPlayerIsOnPitch()
        {
            var stubPlayer = new Player(777, "John", "Test", 3, new finternational_common.Entities.Country(23, "France"), "Test", 777);

            var game = new Game(1, 1, "", 1, 1, "", 0, new DateTime(), false, null, null, null, false);

            var stubFixtureResponse = new FixtureResponse()
            {
                Fixtures = new List<Fixture>()
                {
                    new Fixture()
                    {
                        Information = new FixtureInformation() { Id = 1 },
                        Teams = new Teams()
                        {
                            Home = new Home() { Id = 22 },
                            Away = new Away() { Id = 23 },
                        },
                       Events = new List<Event>()
                       {
                            new Event() { Type = "Goal", Detail = "Normal Goal", Player = new EventPlayer { Id = 666 },  Team = new Team() { Id = 22 }  },
                            new Event() { Type = "Goal", Detail = "Normal Goal", Player = new EventPlayer { Id = 666 },  Team = new Team() { Id = 22 }  },
                            new Event() { Type = "subst", Detail = "Sub 1", Player = new EventPlayer { Id = 777 }, Assist = new EventPlayer { Id = 456 },  Team = new Team() { Id = 22 }  },
                       }
                    }
                }
            };

            var unitOfWork = this.autoMocker.GetMock<IUnitOfWork>();
            var repo = this.autoMocker.GetMock<IPlayerRepository>();

            repo
                .Setup(x => x.GetByExternalId((int)stubPlayer.ExternalId))
                .ReturnsAsync(stubPlayer);

            unitOfWork
                .Setup(x => x.PlayerRepository)
                .Returns(repo.Object);

            var sut = this.autoMocker.CreateInstance<ConcededProvider>();
            var result = await sut.Get(777, stubFixtureResponse, game, 5, unitOfWork.Object);

            Assert.Equal(2, result);
        }

        [Fact]
        public async Task GetReturnsCorrectResultIfPlayerIsSubbed()
        {
            var stubPlayer = new Player(777, "John", "Test", 3, new finternational_common.Entities.Country(23, "France"), "Test", 777);

            var stubFixtureResponse = new FixtureResponse()
            {
                Fixtures = new List<Fixture>()
                {
                    new Fixture()
                    {
                        Information = new FixtureInformation() { Id = 1 },
                        Teams = new Teams()
                        {
                            Home = new Home() { Id = 22 },
                            Away = new Away() { Id = 23 },
                        },
                       Events = new List<Event>()
                       {
                            new Event() { Type = "Goal", Detail = "Normal Goal", Player = new EventPlayer { Id = 666 },  Team = new Team() { Id = 22 }  },
                            new Event() { Type = "subst", Detail = "Sub 1", Player = new EventPlayer { Id = 777 }, Assist = new EventPlayer { Id = 4343443 },  Team = new Team() { Id = 23 }  },
                            new Event() { Type = "Goal", Detail = "Normal Goal", Player = new EventPlayer { Id = 666 },  Team = new Team() { Id = 22 }  },
                       }
                    }
                }
            };

            var game = new Game(1, 1, "", 1, 1, "", 0, new DateTime(), false, null, null, null, false);

            var unitOfWork = this.autoMocker.GetMock<IUnitOfWork>();
            var repo = this.autoMocker.GetMock<IPlayerRepository>();

            repo
                .Setup(x => x.GetByExternalId((int)stubPlayer.ExternalId))
                .ReturnsAsync(stubPlayer);

            unitOfWork
                .Setup(x => x.PlayerRepository)
                .Returns(repo.Object);

            var sut = this.autoMocker.CreateInstance<ConcededProvider>();
            var result = await sut.Get(777, stubFixtureResponse, game, 5, unitOfWork.Object);

            Assert.Equal(1, result);
        }


        [Fact]
        public async Task GetReturnsCorrectResultIfPlayerIsSubbedOnAfterGoal()
        {
            var stubPlayer = new Player(777, "John", "Test", 3, new finternational_common.Entities.Country(23, "France"), "Test", 777);

            var stubFixtureResponse = new FixtureResponse()
            {
                Fixtures = new List<Fixture>()
                {
                    new Fixture()
                    {
                        Information = new FixtureInformation() { Id = 1 },
                        Teams = new Teams()
                        {
                            Home = new Home() { Id = 22 },
                            Away = new Away() { Id = 23 },
                        },
                       Events = new List<Event>()
                       {
                            new Event() { Type = "Goal", Detail = "Normal Goal", Player = new EventPlayer { Id = 666 },  Team = new Team() { Id = 22 }  },
                            new Event() { Type = "subst", Detail = "Sub 1", Player = new EventPlayer { Id = 111 }, Assist = new EventPlayer { Id = 777 },  Team = new Team() { Id = 23 }  },
                            new Event() { Type = "Goal", Detail = "Normal Goal", Player = new EventPlayer { Id = 666 },  Team = new Team() { Id = 22 }  },
                            new Event() { Type = "Goal", Detail = "Normal Goal", Player = new EventPlayer { Id = 666 },  Team = new Team() { Id = 22 }  },
                       }
                    }
                }
            };

            var game = new Game(1, 1, "", 1, 1, "", 0, new DateTime(), false, null, null, null, false);

            var unitOfWork = this.autoMocker.GetMock<IUnitOfWork>();
            var repo = this.autoMocker.GetMock<IPlayerRepository>();

            repo
                .Setup(x => x.GetByExternalId((int)stubPlayer.ExternalId))
                .ReturnsAsync(stubPlayer);

            unitOfWork
                .Setup(x => x.PlayerRepository)
                .Returns(repo.Object);

            var sut = this.autoMocker.CreateInstance<ConcededProvider>();
            var result = await sut.Get(777, stubFixtureResponse, game, 5, unitOfWork.Object);

            Assert.Equal(2, result);
        }

    }
}
