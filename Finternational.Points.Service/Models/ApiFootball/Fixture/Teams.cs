﻿using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Fixture
{
    public class Teams
    {
        [JsonPropertyName("home")]
        public Home? Home { get; set; }
        [JsonPropertyName("away")]
        public Away? Away { get; set; }
    }
}
