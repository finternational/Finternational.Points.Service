﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finternational.Points.Service.Exceptions
{
    public class ApiCallException : Exception
    {
        public ApiCallException(string information, int statusCode, string Url)
        {
            this.Information = information;
            this.StatusCode = statusCode;
            this.Url = Url;
        }

        public string Information { get; set; }
        public int StatusCode { get; set; }
        public string Url { get; set; }
    }
}
