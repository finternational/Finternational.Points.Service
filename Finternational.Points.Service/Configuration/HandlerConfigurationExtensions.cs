﻿using Finternational.Points.Service.Handlers;

namespace Finternational.Points.Service.Configuration
{
    public static class HandlerConfigurationExtensions
    {
        public static void AddHandlers(this IServiceCollection services)
        {
            services.AddTransient<IFixtureHandler, FixtureHandler>();
            services.AddTransient<IGameweekRequiringAutoSubsHandler, GameweekRequiringAutoSubsHandler>();
        }
    }
}
