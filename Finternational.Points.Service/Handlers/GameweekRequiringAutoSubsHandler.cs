﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Handlers
{
    public class GameweekRequiringAutoSubsHandler : IGameweekRequiringAutoSubsHandler
    {
        public async Task Handle(int gameweekId, IUnitOfWork unitOfWork)
        {
            var entries = await unitOfWork.EntryRepository(Guid.Empty).GetAllByGameweek(gameweekId);
            var performances = await unitOfWork.PerformanceRepository.GetByGameweekId(gameweekId);
            var players = await unitOfWork.PlayerRepository.Get(2500, 1, null, null);
            var playerCounts = new List<int>() { 0, 0, 0, 0 };

            foreach (var entry in entries)
            {
                var extendedEntry = await unitOfWork.EntryRepository(entry.TeamId).GetByGameweekId(gameweekId);
                var things = new List<Thing>();


                var extendedEntryPlayers = extendedEntry.Players.OrderBy(x => x.SquadPosition).ToList();

                foreach (var ep in extendedEntryPlayers)
                {
                    var player = players.Where(x => x.Id == ep.PlayerId).First();
                    var performance = performances.Where(x => x.Player == ep.PlayerId).FirstOrDefault();

                    if (!ep.Substitute)
                    {
                        playerCounts[player.Position - 1]++;
                    }

                    things.Add(new Thing(ep.PlayerId, player.Position, performance?.Minutes ?? 0, ep.Substitute, player.Name));
                }

                var subs = things.Where(x => x.Sub).ToList();

                foreach(var sub in subs)
                {
                    var firstTeam = things.Where(x => !x.Sub).ToList();
                    if (sub.Minutes > 0)
                    {
                        foreach(var ft in firstTeam)
                        {
                            if(ft.Minutes == 0)
                            {
                                if(sub.Position == ft.Position)
                                {
                                    SubPlayers(extendedEntryPlayers, sub.Id, ft.Id, things);
                                    break;
                                }
                                else if(sub.Position == 1)
                                {
                                    break;
                                }
                                else
                                {
                                    bool canBetransferred = false;
                                   
                                    switch (ft.Position) //revisit this
                                    {
                                        case 1:
                                            break;
                                        case 2:
                                            canBetransferred = playerCounts[ft.Position - 1] > 3;
                                            break;
                                        case 3:
                                            canBetransferred = playerCounts[ft.Position - 1] > 2;
                                            break;
                                        case 4:
                                            canBetransferred = playerCounts[ft.Position - 1] > 1;
                                            break;                        
                                    }
                                    
                                    if (canBetransferred)
                                    {
                                        SubPlayers(extendedEntryPlayers, sub.Id, ft.Id, things);
                                        break;
                                    }  
                                }
                            }
                        }
                    }
                }

                /*var cIndex = extendedEntryPlayers.IndexOf(extendedEntryPlayers.Find(x => x.Captain));
                var vcIndex = extendedEntryPlayers.IndexOf(extendedEntryPlayers.Find(x => x.ViceCaptain));

                var cPerformance = performances.Where(x => x.Player == extendedEntryPlayers[cIndex].PlayerId).First();
                var vcPerformance = performances.Where(x => x.Player == extendedEntryPlayers[vcIndex].PlayerId).First();

                if(cPerformance.Minutes == 0)
                {
                    if(vcPerformance.Minutes > 0)
                    {
                        extendedEntryPlayers[cIndex] = extendedEntryPlayers[cIndex] with { Captain = false };
                        extendedEntryPlayers[vcIndex] = extendedEntryPlayers[vcIndex] with 
                        { 
                            Captain = true, 
                            ViceCaptain = false, 
                            Points = extendedEntryPlayers[vcIndex].Points * 2 
                        };
                    }
                }*/


                extendedEntry.Players = extendedEntryPlayers;

                await unitOfWork.EntryRepository(extendedEntry.TeamId).Update(extendedEntry);
            }
        }

        private void SubPlayers(List<EntryPlayer> entryPlayers, int subId, int playerId, List<Thing> things)
        {
            var subIndex = entryPlayers.IndexOf(entryPlayers.First(x => x.PlayerId == subId));
            var playerIndex = entryPlayers.IndexOf(entryPlayers.First(x => x.PlayerId == playerId));
            var thingDex = things.IndexOf(things.First(x => x.Id == playerId));
            entryPlayers[subIndex] = entryPlayers[subIndex] with { Substitute = false };
            entryPlayers[playerIndex] = entryPlayers[playerIndex] with { Substitute = true };
            things[thingDex] = things[thingDex] with { Sub = true };
        }
    }

    public record Thing(int Id, int Position, int Minutes, bool Sub, string name)
    {
    }
}