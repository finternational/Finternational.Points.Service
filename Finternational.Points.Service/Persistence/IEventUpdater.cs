﻿using Finternational.Points.Service.Models.ApiFootball.Fixture;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Persistence
{
    public interface IEventUpdater
    {
        Task UpdateEvents(FixtureResponse fixture, IUnitOfWork unitOfWork);
    }
}
