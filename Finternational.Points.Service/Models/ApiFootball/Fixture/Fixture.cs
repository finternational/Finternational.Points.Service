﻿using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Fixture
{
    public class Fixture
    {
        [JsonPropertyName("fixture")]
        public FixtureInformation? Information { get; set; }
        [JsonPropertyName("league")]
        public League? League { get; set; }
        [JsonPropertyName("teams")]
        public Teams? Teams { get; set; }
        [JsonPropertyName("goals")]
        public Goals? Goals { get; set; }
        [JsonPropertyName("score")]
        public Score? Score { get; set; }
        [JsonPropertyName("events")]
        public List<Event> Events { get; set; }
        [JsonPropertyName("players")]
        public List<PlayerResponse> Players { get; set; }
        [JsonPropertyName("lineups")]
        public List<Root> LineUps { get; set; }
    }

    public class LineUpPlayer
    {
        public int id { get; set; }
        public string name { get; set; }
        public int number { get; set; }
        public string pos { get; set; }
        public object grid { get; set; }
    }

    public class Root
    {
        public List<Substitute> startXI { get; set; }
        public List<Substitute> substitutes { get; set; }
    }

    public class Substitute
    {
        public LineUpPlayer player { get; set; }
    }
}
