﻿using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Fixture
{
    public class Statistic
    {
        [JsonPropertyName("type")]
        public string Type { get; set; }
        [JsonPropertyName("games")]
        public PlayerGame? Games { get; set; }
        [JsonPropertyName("offsides")]
        public int? Offsides { get; set; }
        [JsonPropertyName("goals")]
        public PlayerGoals? Goals { get; set; }
        [JsonPropertyName("cards")]
        public Cards? Cards { get; set; }
        [JsonPropertyName("penalty")]
        public Penalty? Penalty { get; set; }
    }
}
