﻿using Finternational.Points.Service.Models.ApiFootball.Fixture;
using Finternational.Points.Service.Persistence;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Handlers
{
    public class FixtureHandler : IFixtureHandler
    {
        private readonly ILogger<FixtureHandler> logger;
        private readonly IGameUpdater gameUpdater;
        private readonly IPerformanceUpdater performanceUpdater;
        private readonly IEventUpdater eventUpdater;

        public FixtureHandler(
            ILogger<FixtureHandler> logger,
            IGameUpdater gameUpdater, IPerformanceUpdater performanceUpdater, IEventUpdater eventUpdater)
        {
            this.logger = logger;
            this.gameUpdater = gameUpdater;
            this.performanceUpdater = performanceUpdater;
            this.eventUpdater = eventUpdater;
        }

        public async Task Handle(FixtureResponse fixture, List<Game> games, IUnitOfWork unitOfWork)
        {
            try
            {
                var game = games.Where(x => x.Id == fixture.Fixtures[0].Information?.Id).First();

                if (game.Finished)
                {
                    //check if anything has changed, write to manual update table
                    //increment next check
                    //autosubs?
                }
                else if (game.FinishedProvisional ?? false)
                {
                    var gameUpdateTask = this.gameUpdater.UpdateProvisionallyFinishedGame(fixture, game, unitOfWork);
                    var performanceUpdateTask = this.performanceUpdater.UpdatePerformances(fixture, game, unitOfWork);
                    var eventUpdateTask = this.eventUpdater.UpdateEvents(fixture, unitOfWork);

                    await performanceUpdateTask;
                    await gameUpdateTask;
                    await eventUpdateTask;
                }
                else
                {
                    var gameUpdateTask = this.gameUpdater.UpdateInPlayGame(fixture, game, unitOfWork);
                    var performanceUpdateTask = this.performanceUpdater.UpdatePerformances(fixture, game, unitOfWork);
                    var eventUpdateTask = this.eventUpdater.UpdateEvents(fixture, unitOfWork);

                    await performanceUpdateTask;
                    await gameUpdateTask;
                    await eventUpdateTask;
                }
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
            }
        }
    }
}
