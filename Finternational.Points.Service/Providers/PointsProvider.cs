﻿using finternational_common.Entities;
using finternational_common.Enums;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Providers
{
    public class PointsProvider : IPointsProvider
    {
        public async Task<int> Get(Performance performance, IUnitOfWork unitOfWork)
        {
            var player = await unitOfWork.PlayerRepository.GetById(performance.Player);
            var eventPoints = await unitOfWork.EventRepository.GetEventPoints();
            var points = 0;

            if (performance.Minutes >= 60)
            {
                points += eventPoints.Where(x => x.Id == (int)EventTypeEnum.SixtyMinutes).First().Points;
            }
            if (performance.Minutes > 0)
            {
                points += eventPoints.Where(x => x.Id == (int)EventTypeEnum.SixtyPlusMinutes).First().Points;
            }
            if (performance.CleanSheet)
            {
                points += eventPoints.Where(x => x.Name == "Clean sheet" && x.Position == player.Position).FirstOrDefault()?.Points ?? 0;
            }
            if (performance.YellowCard > 0 || performance.RedCard > 0)
            {
                if(performance.RedCard > 0)
                {
                    points += eventPoints.Where(x => x.Id == (int)EventTypeEnum.RedCard).First().Points;
                }
                else
                {
                    points += eventPoints.Where(x => x.Id == (int)EventTypeEnum.YellowCard).First().Points;
                }                
            }

            points += (performance.Goals * eventPoints.Where(x => x.Name == "Goal" && x.Position == player.Position).First().Points);
            points += (performance.Assists * eventPoints.Where(x => x.Id == (int)EventTypeEnum.Assist).First().Points);
            points += (performance.PenaltySaved * eventPoints.Where(x => x.Id == (int)EventTypeEnum.PenaltySave).First().Points);
            points += (performance.PenaltyWon * eventPoints.Where(x => x.Id == (int)EventTypeEnum.PenaltyWon).First().Points);
            points += (performance.PenaltyMissed * eventPoints.Where(x => x.Id == (int)EventTypeEnum.PenaltyMiss).First().Points);
            points += (performance.OwnGoal * eventPoints.Where(x => x.Id == (int)EventTypeEnum.OwnGoal).First().Points);
            points += (int)Math.Floor((double)performance.Saves / 3) * eventPoints.Where(x => x.Id == (int)EventTypeEnum.Saves).First().Points;

            //change this to like other positions
            if(player.Position == 1 || player.Position == 2)
            {
                points += (int)Math.Floor((double)performance.Conceded / 2) * eventPoints.Where(x => x.Id == (int)EventTypeEnum.Conceded).First().Points;
            }

            return points;
        }
    }
}