﻿using Finternational.Points.Service.Models.ApiFootball.Fixture;
using finternational_common.Entities;
using finternational_common.Enums;
using finternational_common.Persistence.Interfaces;
using Event = finternational_common.Entities.Event;
using Player = finternational_common.Entities.Player;
using ApiPlayer = Finternational.Points.Service.Models.ApiFootball.Fixture.Player;
using ApiEvent = Finternational.Points.Service.Models.ApiFootball.Fixture.Event;
using Finternational.Points.Service.Exceptions;


//this is horrible, refactor
namespace Finternational.Points.Service.Persistence
{
    public class EventUpdater : IEventUpdater
    {
        private readonly ILogger<EventUpdater> logger;

        public EventUpdater(ILogger<EventUpdater> logger)
        {
            this.logger = logger;
        }

        public async Task UpdateEvents(FixtureResponse fixture, IUnitOfWork unitOfWork)
        {
            var fixtureId = (int)fixture.Fixtures[0].Information.Id;
            var homeTeam = (int)fixture.Fixtures[0].Teams.Home.Id;
            var awayTeam = (int)fixture.Fixtures[0].Teams.Away.Id;

            await unitOfWork.EventRepository.DeleteByGameId(fixtureId);

            var players = await unitOfWork.PlayerRepository.Get(2000, 1, null, null);
            //TODO: refactor how events work, so we don't need to do this, events have a type, position in a link table / another column

            var events = new List<Event>();

            await AddPlayerStatisticEvents(fixture.Fixtures[0].Players[0].Players, homeTeam, fixtureId, events, players, unitOfWork);
            await AddPlayerStatisticEvents(fixture.Fixtures[0].Players[1].Players, awayTeam, fixtureId, events, players, unitOfWork);
            await AddGameEvents(fixture.Fixtures[0].Events, events, unitOfWork, fixtureId, players);

            await unitOfWork.EventRepository.Insert(events);
        }

        private async Task AddPlayerStatisticEvents(List<ApiPlayer> apiPlayers, int teamId, int fixtureId, List<Event> events, List<Player> players, IUnitOfWork unitOfWork)
        {
            foreach (var player in apiPlayers)
            {
                try
                {
                    var dbPlayer = await GetPlayer((int)player.Details.Id, player.Details.name, teamId, players, unitOfWork);
                    await AddPlayerStatisticEvent(dbPlayer, EventTypeEnum.PenaltySave, player.Statistics?[0]?.Penalty?.Saved ?? 0, teamId, fixtureId, events, players, unitOfWork);
                    await AddPlayerStatisticEvent(dbPlayer, EventTypeEnum.PenaltyMiss, player.Statistics?[0]?.Penalty?.Missed ?? 0, teamId, fixtureId, events, players, unitOfWork);
                    await AddPlayerStatisticEvent(dbPlayer, EventTypeEnum.PenaltyWon, player.Statistics?[0]?.Penalty?.Won ?? 0, teamId, fixtureId, events, players, unitOfWork);
                    await AddPlayerStatisticEvent(dbPlayer, EventTypeEnum.Saves, player.Statistics?[0]?.Goals?.Saves ?? 0, teamId, fixtureId, events, players, unitOfWork);
                }
                catch
                {
                    this.logger.LogError($"Failed to add stastistic event for player {player.Details?.Id} in game ${fixtureId}");
                }
            }
        }

        private async Task AddPlayerStatisticEvent(Player player, EventTypeEnum e, int count, int countryId, int gameId, List<Event> events, List<Player> players, IUnitOfWork unitOfWork)
        {
            for (int i = 0; i < count; i++)
            {
                events.Add(new Event(0, (int)e, player.Id, countryId, gameId));
            }
        }

        private async Task AddGameEvents(List<ApiEvent> apiEvents, List<Event> events, IUnitOfWork unitOfWork, int fixtureId, List<Player> players)
        {
            //THINK ABOUT REMOVING THIS AT SOME POINT, EVENTS THEMSELVES DONT NEED A POSITION
            var eventPoints = await unitOfWork.EventRepository.GetEventPoints();
            //TODO: implement a 'DATABASE ENTITY' so we can do multiple async db calls nicely?

            foreach (var e in apiEvents)
            {
                try
                {
                    var player = await GetPlayer((int)e.Player.Id, e.Player?.Name, (int)e.Team.Id, players, unitOfWork);
                    if (e.Type == "Goal")
                    {
                        if (e.Detail == "Normal Goal")
                        {
                            events.Add(new Event(0, GetGoalEventId(player, eventPoints), player.Id, (int)e.Team.Id, fixtureId));

                            if (e.Assist?.Id != null)
                            {
                                try
                                {
                                    var assistPlayer = await GetPlayer((int)e.Assist.Id, e.Assist.Name, (int)e.Team.Id, players, unitOfWork);
                                    events.Add(new Event(0, (int)EventTypeEnum.Assist, assistPlayer.Id, (int)e.Team.Id, fixtureId));
                                }
                                catch (Exception ex)
                                {
                                    this.logger.LogError($"Failed to add event assist for player {e.Assist.Id} in game ${fixtureId}");
                                }                           
                            }
                        }
                        else if (e.Detail == "Penalty" && e.Time.Elapsed != 120)
                        {
                            events.Add(new Event(0, GetGoalEventId(player, eventPoints), player.Id, (int)e.Team.Id, fixtureId));
                        }
                        else if (e.Detail == "Own Goal")
                        {
                            events.Add(new Event(0, (int)EventTypeEnum.OwnGoal, player.Id, (int)e.Team.Id, fixtureId));
                        }
                    }
                    else if (e.Type == "Card")
                    {
                        if (e.Detail == "Yellow Card")
                        {
                            events.Add(new Event(0, (int)EventTypeEnum.YellowCard, player.Id, (int)e.Team.Id, fixtureId));
                        }
                        else if (e.Detail == "Red Card")
                        {
                            events.Add(new Event(0, (int)EventTypeEnum.RedCard, player.Id, (int)e.Team.Id, fixtureId));
                        }
                    }
                }
                catch
                {
                    this.logger.LogError($"Failed to add event {e.Type} {e.Detail} for player {e.Player?.Id} in game ${fixtureId}");
                }
            }
        }

        private int GetGoalEventId(Player player, List<EventPoints> eventPoints)
        {
            return eventPoints.Where(x => x.Name == "Goal" && x.Position == player.Position).First().Id;
        }

        private async Task<Player> GetPlayer(int playerId, string playerName, int teamId, List<Player> databasePlayers, IUnitOfWork unitOfWork)
        {
            var player = databasePlayers.Where(x => x.Id == playerId).FirstOrDefault();

            if (player == null)
            {
                this.logger.LogCritical($"Player ID: {playerId} Name: {playerName} was not found in the DB by Id");
            }
            else
            {
                return player;
            }

            player = databasePlayers.Where(x => x.ExternalId == playerId).FirstOrDefault();

            if (player == null)
            {
                this.logger.LogCritical($"Player ID: {playerId} Name: {playerName} was not found in the by external id");
                await unitOfWork.MissingPlayerRepository.Insert(new MissingPlayer(playerName, teamId, playerId));
                throw new PlayerMissingException();
            }
            else
            {
                return player;
            }
        }
    }
}