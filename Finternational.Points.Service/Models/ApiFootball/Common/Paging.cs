﻿using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Common
{
    public class Paging
    {
        [JsonPropertyName("current")]
        public int? Current { get; set; }
        [JsonPropertyName("total")]
        public int? Total { get; set; }
    }
}
