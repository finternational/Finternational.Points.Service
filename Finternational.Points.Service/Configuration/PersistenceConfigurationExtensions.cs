﻿using Finternational.Points.Service.Persistence;
using finternational_common.Persistence;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Configuration
{
    public static class PersistenceConfigurationExtensions
    {
        public static void AddPersistenceLayer(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWorkProvider, UnitOfWorkProvider>();
            services.AddTransient<IConnectionProvider, ConnectionProvider>();
            services.AddTransient<IPerformanceUpdater, PerformanceUpdater>();
            services.AddTransient<IEventUpdater, EventUpdater>();
            services.AddTransient<IGameUpdater, GameUpdater>();
        }
    }
}
