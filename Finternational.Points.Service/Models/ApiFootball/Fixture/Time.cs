﻿using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Fixture
{
    public class Time
    {
        [JsonPropertyName("elapsed")]
        public int? Elapsed { get; set; }
        [JsonPropertyName("extra")]
        public int? Extra { get; set; }
    }
}
