﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Providers
{
    public class GamesToCheckProvider : IGamesToCheckProvider
    {
        private readonly IDateTimeProvider dateTimeProvider;

        public GamesToCheckProvider(IDateTimeProvider dateTimeProvider)
        {
            this.dateTimeProvider = dateTimeProvider;
        } 

        public async Task<List<Game>> Get(IUnitOfWork unitOfWork)
        {
            var games = await unitOfWork.GameRepository.Get();
            return games.Where(x =>
               this.dateTimeProvider.Now() > x.KickOff &&
               (x.NextCheck == null || this.dateTimeProvider.Now() > x.NextCheck) && !x.Finished).ToList();
        }
    }
}
