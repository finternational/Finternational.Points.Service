﻿using Finternational.Points.Service.Models.ApiFootball.Fixture;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Persistence
{
    public interface IPerformanceUpdater
    {
        Task UpdatePerformances(FixtureResponse fixture, Game game, IUnitOfWork unitOfWork);
    }
}
