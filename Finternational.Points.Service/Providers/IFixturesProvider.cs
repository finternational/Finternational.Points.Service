﻿using Finternational.Points.Service.Models.ApiFootball.Fixture;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Providers
{
    public interface IFixturesProvider
    {
        Task<List<FixtureResponse>> Get(List<Game> games, IUnitOfWork unitOfWork);
    }
}
