﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Moq;
using Moq.AutoMock;
using System.Collections.Generic;
using Xunit;

namespace Finternational.Points.Service.Tests.Handlers
{
    public class GameweekRequiringAutoSubsHandlerTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public void HandleSavesCorrectResults()
        {
            var entry = new Entry()
            {
                GameweekId = 1,
                Players = new List<EntryPlayer>()
                {
                    new EntryPlayer(true, false, false, 1, 1, null, null, 0), //not subbed out, keeper didnt play
                    new EntryPlayer(false, true, false, 2, 2,null, null, 5),
                    new EntryPlayer(false, false, false, 3, 3, null, null, 0), //not subbed out for attackers
                    new EntryPlayer(false, false, false, 4, 4, null, null, 0),
                    new EntryPlayer(false, false, false, 5, 5, null, null, 0),
                    new EntryPlayer(false, false, false, 6, 6, null, null, 0),
                    new EntryPlayer(false, false, false, 7, 7, null, null, 1),
                    new EntryPlayer(false, false, false, 8, 8, null, null, 1),
                    new EntryPlayer(false, false, false, 9, 9, null, null, 1),
                    new EntryPlayer(false, false, false, 10, 10, null, null, 1),
                    new EntryPlayer(false, false, false, 11, 11, null, null, 1),
                    new EntryPlayer(false, false, true, 13, 12, null, null, 1), //mid
                    new EntryPlayer(false, false, true, 14, 13, null, null, 0), //def
                    new EntryPlayer(false, false, true, 15, 14, null, null, 3), //atk
                }
            };
            var gameweekId = 1;
            var unitOfWork = this.autoMocker.GetMock<IUnitOfWork>();
            var entryRepository = this.autoMocker.GetMock<IEntryRepository>();
            entryRepository
                .Setup(x => x.GetAllByGameweek(gameweekId))
                .ReturnsAsync(new List<Entry> { entry });
            var performanceRepository = this.autoMocker.GetMock<IPerformanceRepository>();
            performanceRepository
                .Setup(x => x.GetByGameweekId(gameweekId))
                .ReturnsAsync(new List<Performance>()
                {
                    new Performance(1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0),
                    new Performance(1, 2, 1, 5, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0),
                    new Performance(1, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0),
                    new Performance(1, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0),
                    new Performance(1, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0),
                    new Performance(1, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0),
                    new Performance(1, 7, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0),
                    new Performance(1, 8, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0),
                    new Performance(1, 9, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0),
                    new Performance(1, 10, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0),
                    new Performance(1, 11, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0),
                    new Performance(1, 12, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0),
                    new Performance(1, 13, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0),
                    new Performance(1, 14, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0),
                    new Performance(1, 15, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0),
                });
            var playerRepo = this.autoMocker.GetMock<IPlayerRepository>();
            playerRepo
                .Setup(x => x.Get(2500, 1, null, null))
                .ReturnsAsync(new List<Player>()
                {
                    new Player(1, "", "", 1, null, "", 1),
                    new Player(2, "", "", 2, null, "", 1),
                    new Player(3, "", "", 2, null, "", 1),
                    new Player(4, "", "", 2, null, "", 1),
                    new Player(5, "", "", 2, null, "", 1),
                    new Player(6, "", "", 1, null, "", 1),
                    new Player(7, "", "", 1, null, "", 1),
                    new Player(8, "", "", 1, null, "", 1),
                    new Player(9, "", "", 1, null, "", 1),
                    new Player(10, "", "", 1, null, "", 1),
                    new Player(11, "", "", 1, null, "", 1),
                    new Player(12, "", "", 1, null, "", 1),
                    new Player(13, "", "", 1, null, "", 1),
                    new Player(14, "", "", 2, null, "", 1),
                    new Player(15, "", "", 1, null, "", 1),

                });

            

        }

    }
}
