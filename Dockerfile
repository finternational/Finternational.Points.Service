FROM mcr.microsoft.com/dotnet/sdk:6.0 AS builder
WORKDIR /src
EXPOSE 80
COPY . .

#Restore Dependencies
RUN dotnet restore "Finternational.Points.Service/Finternational.Points.Service.csproj"  -s "https://www.nuget.org/api/v2/" -s "https://gitlab.com/api/v4/projects/35958452/packages/nuget/index.json"
RUN dotnet restore "Finternational.Points.Service.Tests/Finternational.Points.Service.Tests.csproj" -s "https://www.nuget.org/api/v2/" 
#Run Tests
RUN dotnet test "Finternational.Points.Service.Tests/Finternational.Points.Service.Tests.csproj" /p:CollectCoverage=true
#Publish App
RUN dotnet publish "Finternational.Points.Service/Finternational.Points.Service.csproj" -c Release -o /app/publish
FROM mcr.microsoft.com/dotnet/sdk:6.0  AS final
#Copy App and Run
WORKDIR /app
ENV ASPNETCORE_URLS=http://+:80
COPY --from=builder /app/publish /app
ENTRYPOINT ["dotnet", "finternational-points-service.dll"]
