﻿using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Providers
{
    public interface IGamesToCheckProvider
    {
        Task<List<Game>> Get(IUnitOfWork unitOfWork);
    }
}
