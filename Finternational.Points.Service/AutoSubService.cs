﻿using Finternational.Points.Service.Configuration;
using Finternational.Points.Service.Handlers;
using Finternational.Points.Service.Providers;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service
{
    public class AutoSubService : BackgroundService
    {
        private readonly ILogger<AutoSubService> logger;
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private readonly PersistenceSettings persistenceSettings;
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly IGameweekRequiringAutoSubsHandler gameweekRequiringAutoSubsHandler;
        private readonly IDelayProvider delayProvider;

        public AutoSubService(
            ILogger<AutoSubService> logger,
            IUnitOfWorkProvider unitOfWorkProvider, 
            PersistenceSettings persistenceSettings,
            IDateTimeProvider dateTimeProvider,
            IGameweekRequiringAutoSubsHandler gameweekRequiringAutoSubsHandler,
            IDelayProvider delayProvider)
        {
            this.logger = logger;
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.persistenceSettings = persistenceSettings;
            this.dateTimeProvider = dateTimeProvider;
            this.gameweekRequiringAutoSubsHandler = gameweekRequiringAutoSubsHandler;
            this.delayProvider = delayProvider;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                using(var unitOfWork = this.unitOfWorkProvider.CreateWithoutOpenTransaction(this.persistenceSettings.ConnectionString))
                {
                    var gameweeks = await unitOfWork.GameweekRepository.Get(1000, 1);
                    logger.LogInformation($"Found {gameweeks.Count} gameweeks.");

                    var gameweeksNeedingAutosubs = gameweeks.Where(x => this.dateTimeProvider.Now() > x.End && !x.Autosubs);
                    logger.LogInformation($"Found {gameweeksNeedingAutosubs.Count()} gameweeks needing autoSubs.");

                    foreach (var gw in gameweeksNeedingAutosubs)
                    {
                        await this.gameweekRequiringAutoSubsHandler.Handle(gw.Id, unitOfWork);
                        unitOfWork.GameweekRepository.Update(gw with { Autosubs = true });
                    }

                    if (gameweeksNeedingAutosubs.Any())
                    {
                        logger.LogInformation($"Committing autosubs");
                        await unitOfWork.Commit();
                    }

                    logger.LogInformation($"Sleeping for 5 minutes");
                    await this.delayProvider.DelayMinutes(stoppingToken, 5);
                }
            }
        }
    }
}
