﻿using Finternational.Points.Service.Providers;

namespace Finternational.Points.Service.Configuration
{
    public static class ProviderConfigurationExtensions
    {
        public static void AddProviders(this IServiceCollection services)
        {
            services.AddTransient<IDateTimeProvider, DateTimeProvider>();
            services.AddTransient<IPointsProvider, PointsProvider>();
            services.AddTransient<IFixturesProvider, FixturesProvider>();
            services.AddTransient<IGamesToCheckProvider, GamesToCheckProvider>();
            services.AddTransient<IDelayProvider, DelayProvider>();
            services.AddTransient<IConcededProvider, ConcededProvider>();
        }
    }
}
