﻿using Finternational.Points.Service.Configuration;
using Finternational.Points.Service.Handlers;
using Finternational.Points.Service.Models.ApiFootball.Common;
using Finternational.Points.Service.Models.ApiFootball.Fixture;
using Finternational.Points.Service.Providers;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using Moq;
using Moq.AutoMock;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Finternational.Points.Service.Tests
{
    public class PointsServiceTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task ExecuteAsyncSavesChanges()
        {
            CancellationTokenSource source = new CancellationTokenSource();
            var stubGames = new List<Game>();
            var stubFixtures = new List<FixtureResponse>()
            {
                new FixtureResponse() { Parameters = new Parameters() { Id = "1"} },
                new FixtureResponse() { Parameters = new Parameters() { Id = "2"} }
            };

            var stubSettings = new PersistenceSettings("conString");
            
            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            this.autoMocker.Use(stubSettings);
            this.autoMocker.GetMock<IGamesToCheckProvider>()
                .Setup(x => x.Get(mockUnitOfWork.Object))
                .ReturnsAsync(stubGames);
            this.autoMocker.GetMock<IFixturesProvider>()
                .Setup(x => x.Get(stubGames, mockUnitOfWork.Object))
                .ReturnsAsync(stubFixtures);

            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.CreateWithoutOpenTransaction(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);

            var sut = this.autoMocker.CreateInstance<PointService>();

            await sut.Execute(source.Token);

            this.autoMocker.GetMock<IFixtureHandler>()
                .Verify(x => x.Handle(It.IsAny<FixtureResponse>(), stubGames, mockUnitOfWork.Object), Times.Exactly(2));
        }

        [Fact]
        public async Task ExecuteSleepsFor60MinutesIfSaveFails()
        {
            CancellationTokenSource source = new CancellationTokenSource();
            var stubGames = new List<Game>();
            var stubFixtures = new List<FixtureResponse>()
            {
                new FixtureResponse() { Parameters = new Parameters() { Id = "1"} },
                new FixtureResponse() { Parameters = new Parameters() { Id = "2"} }
            };

            var stubSettings = new PersistenceSettings("conString");

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            this.autoMocker.Use(stubSettings);
            this.autoMocker.GetMock<IGamesToCheckProvider>()
                .Setup(x => x.Get(mockUnitOfWork.Object))
                .ReturnsAsync(stubGames);
            this.autoMocker.GetMock<IFixturesProvider>()
                .Setup(x => x.Get(stubGames, mockUnitOfWork.Object))
                .ReturnsAsync(stubFixtures);
            this.autoMocker.GetMock<IUnitOfWorkProvider>()
                .Setup(x => x.CreateWithoutOpenTransaction(stubSettings.ConnectionString))
                .Returns(mockUnitOfWork.Object);
            mockUnitOfWork
                .Setup(x => x.Commit())
                .ThrowsAsync(new Exception());

            var sut = this.autoMocker.CreateInstance<PointService>();
            await sut.Execute(source.Token);

            this.autoMocker.GetMock<IDelayProvider>()
                .Verify(x => x.DelayMinutes(source.Token, 60), Times.Once);
        }
    }
}
