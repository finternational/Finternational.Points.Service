﻿using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Fixture
{
    public class Periods
    {
        [JsonPropertyName("first")]
        public int? First { get; set; }
        [JsonPropertyName("second")]
        public int? Second { get; set; }
    }
}
