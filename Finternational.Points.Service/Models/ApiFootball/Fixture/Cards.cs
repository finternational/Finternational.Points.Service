﻿using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Fixture
{
    public class Cards
    {
        [JsonPropertyName("yellow")]
        public int? Yellow { get; set; }
        [JsonPropertyName("red")]
        public int? Red { get; set; }
    }
}
