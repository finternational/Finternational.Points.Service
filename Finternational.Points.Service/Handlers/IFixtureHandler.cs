﻿using Finternational.Points.Service.Models.ApiFootball.Fixture;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Handlers
{
    public interface IFixtureHandler
    {
        Task Handle(FixtureResponse fixture, List<Game> games, IUnitOfWork unitOfWork);
    }
}
