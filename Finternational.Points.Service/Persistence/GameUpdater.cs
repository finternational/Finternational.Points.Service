﻿using Finternational.Points.Service.Models.ApiFootball.Fixture;
using Finternational.Points.Service.Providers;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Persistence
{
    public class GameUpdater : IGameUpdater
    {
        private readonly IDateTimeProvider dateTimeProvider;

        public GameUpdater(IDateTimeProvider dateTimeProvider)
        {
            this.dateTimeProvider = dateTimeProvider;
        }

        public async Task UpdateInPlayGame(FixtureResponse fixture, Game game, IUnitOfWork unitOfWork)
        {
            var updatedGame = game with
            {
                HomeGoals = fixture.Fixtures[0]?.Goals?.Home ?? 0,
                AwayGoals = fixture.Fixtures[0]?.Goals?.Away ?? 0,
                FinishedProvisional = GameFinishedProvisional(fixture.Fixtures[0].Information?.Status?.Long),
                FinishedTime = GameFinishedProvisional(fixture.Fixtures[0].Information?.Status?.Long) ? this.dateTimeProvider.Now() : game.FinishedTime
            };

            if(updatedGame != game)
            {
                await unitOfWork.GameRepository.Update(updatedGame);
            };
        }
                
        public async Task UpdateProvisionallyFinishedGame(FixtureResponse fixture, Game game, IUnitOfWork unitOfWork)
        {
            var updatedGame = game with
            {
                HomeGoals = fixture.Fixtures[0]?.Goals?.Home ?? 0,
                AwayGoals = fixture.Fixtures[0]?.Goals?.Away ?? 0,
                Finished = GameFinished(game.FinishedTime),
                NextCheck = GameFinished(game.FinishedTime) ? this.dateTimeProvider.Now().AddHours(12)  : this.dateTimeProvider.Now().AddMinutes(20)
            };

            await unitOfWork.GameRepository.Update(updatedGame);
        }

        private bool GameFinishedProvisional(string? status)
        {
            return status != null && status == "Match Finished";
        }

        private bool GameFinished(DateTime? finishedProvisionalTime)
        {
            return finishedProvisionalTime != null && this.dateTimeProvider.Now() > finishedProvisionalTime.Value.AddHours(2);
        }
    }
}
