﻿using Finternational.Points.Service.Models.ApiFootball.Fixture;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Moq.AutoMock;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Player = finternational_common.Entities.Player;
using ApiPlayer = Finternational.Points.Service.Models.ApiFootball.Fixture.Player;
using ApiEvent = Finternational.Points.Service.Models.ApiFootball.Fixture.Event;
using ApiTeam = Finternational.Points.Service.Models.ApiFootball.Fixture.Team;
using Event = finternational_common.Entities.Event;
using Moq;
using Finternational.Points.Service.Persistence;
using finternational_common.Enums;
using finternational_common.Entities;

namespace Finternational.Points.Service.Tests.Persistence
{
    public class EventUpdaterTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task UpdateEventsInsertsCorrectEvents()
        {
            var points = new List<EventPoints>()
            {
                new EventPoints(1, "0-60 minutes", 1, null),
                new EventPoints(2, "60+ minutes", 1, null),
                new EventPoints(3, "Goal", 6, 1),
                new EventPoints(4, "Goal", 6, 2),
                new EventPoints(5, "Goal", 5, 3),
                new EventPoints(6, "Goal", 4, 4),
                new EventPoints(7, "Assist", 3, null),
                new EventPoints(8, "Clean sheet", 4, 1),
                new EventPoints(9, "Clean sheet", 4, 2),
                new EventPoints(10, "Clean sheet", 1, 3),
                new EventPoints(11, "Saves", 1, 1),
                new EventPoints(12, "Penalty Save", 5, 1),
                new EventPoints(13, "Penalty Miss", -2, null),
                new EventPoints(14, "Conceded", -1, null),
                new EventPoints(16, "Yellow card", -1, null),
                new EventPoints(17, "Red Card", -3, null),
                new EventPoints(18, "Own goal", -2, null),
                new EventPoints(19, "Penalty Won", 3, null),
            };

            var stubFixtureResponse = new FixtureResponse()
            {
                Fixtures = new List<Fixture>()
                {
                    new Fixture()
                    {
                        Information = new FixtureInformation() { Id = 1 },
                        Teams = new Teams()
                        {
                            Home = new Home() { Id = 22 },
                            Away = new Away() { Id = 23 },
                        },
                       Events = new List<ApiEvent>()
                       {
                            new ApiEvent() { Type = "Goal", Detail = "Normal Goal", Player = new EventPlayer { Id = 666 },  Team = new ApiTeam() { Id = 22 }  },
                            new ApiEvent() { Type = "Goal", Detail = "Normal Goal", Player = new EventPlayer { Id = 666 }, Assist = new EventPlayer { Id = 999 }, Team = new ApiTeam() { Id = 22 }  },
                            new ApiEvent() { Type = "Goal", Detail = "Normal Goal", Player = new EventPlayer { Id = 999 }, Team = new ApiTeam() { Id = 22 }  },
                            new ApiEvent() { Type = "Goal", Detail = "Normal Goal", Player = new EventPlayer { Id = 333 }, Team = new ApiTeam() { Id = 23 }  },
                            new ApiEvent() { Type = "Goal", Detail = "Own Goal", Player = new EventPlayer { Id = 333 },  Team = new ApiTeam() { Id = 23 }  },
                            new ApiEvent() { Type = "Goal", Detail = "Penalty", Player = new EventPlayer { Id = 333 },  Team = new ApiTeam() { Id = 23 }, Time = new Time() { Elapsed = 115 }  },
                            new ApiEvent() { Type = "Goal", Detail = "Penalty", Player = new EventPlayer { Id = 333 },  Team = new ApiTeam() { Id = 23 }, Time = new Time() { Elapsed = 120 }  },
                            new ApiEvent() { Type = "Card", Detail = "Yellow Card", Player = new EventPlayer { Id = 333 },  Team = new ApiTeam() { Id = 23 }  },
                            new ApiEvent() { Type = "Card", Detail = "Red Card", Player = new EventPlayer { Id = 333 },  Team = new ApiTeam() { Id = 23 }  },

                        },
                        Players = new List<PlayerResponse>()
                        {
                            new PlayerResponse()    //home
                            {
                                Players = new List<ApiPlayer>()
                                {
                                    new ApiPlayer()
                                    {
                                        Details = new PlayerDetails() { Id = 666 },
                                        Statistics = new List<Statistic>()
                                        {
                                            new Statistic()
                                            {
                                                Penalty = new Penalty() { Saved = 1 },
                                                Goals = new PlayerGoals() { Saves = 2 }
                                            }
                                        }
                                    },
                                    new ApiPlayer()
                                    {
                                        Details = new PlayerDetails() { Id = 999 },
                                        Statistics = new List<Statistic>()
                                        {
                                            new Statistic()
                                            {
                                                Penalty = new Penalty() { Won = 1 }
                                            }
                                        }
                                    }
                                }
                            },
                            new PlayerResponse()   //away
                            {
                                Players = new List<ApiPlayer>()
                                {
                                    new ApiPlayer()
                                    {
                                        Details = new PlayerDetails() { Id = 333 },
                                        Statistics = new List<Statistic>()
                                        {
                                            new Statistic()
                                            {
                                                Penalty = new Penalty() { Won = 1, Missed = 1 }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();
            var mockPlayerRepo = this.autoMocker.GetMock<IPlayerRepository>();
            var mockEventRepo = this.autoMocker.GetMock<IEventRepository>();

            var result = new List<Event>();

            var stubPlayers = new List<Player>()
            {
                new Player(666, "John", "Tester", 1, null, "John Tester", 666),
                new Player(999, "John", "Testee", 4, null, "John Testee", 999),
                new Player(33, "John", "Tester", 2, null, "John Tester", 333)
            };

            mockPlayerRepo
                .Setup(x => x.Get(2000, 1, null, null))
                .ReturnsAsync(stubPlayers);
            mockEventRepo
                .Setup(x => x.Insert(It.IsAny<List<Event>>()))
                .Callback<List<Event>>(events => { result = events; });
            mockEventRepo
                .Setup(x => x.GetEventPoints())
                .ReturnsAsync(points);
            mockUnitOfWork
                .Setup(x => x.PlayerRepository)
                .Returns(mockPlayerRepo.Object);
            mockUnitOfWork
                .Setup(x => x.EventRepository)
                .Returns(mockEventRepo.Object);

            var sut = this.autoMocker.CreateInstance<EventUpdater>();

            await sut.UpdateEvents(stubFixtureResponse, mockUnitOfWork.Object);

            mockUnitOfWork
                .Verify(x => x.EventRepository.DeleteByGameId(1), Times.Once);

            var expectResult = new List<Event>()
            {
                new Event(0, (int)EventTypeEnum.PenaltySave, 666, 22, 1),
                new Event(0, (int)EventTypeEnum.Saves, 666, 22, 1),
                new Event(0, (int)EventTypeEnum.Saves, 666, 22, 1),
                new Event(0, (int)EventTypeEnum.PenaltyWon, 999, 22, 1),
                new Event(0, (int)EventTypeEnum.PenaltyMiss, 33, 23, 1),
                new Event(0, (int)EventTypeEnum.PenaltyWon, 33, 23, 1),
                new Event(0, (int)EventTypeEnum.GoalkeeperGoal, 666, 22, 1),
                new Event(0, (int)EventTypeEnum.GoalkeeperGoal, 666, 22, 1),
                new Event(0, (int)EventTypeEnum.Assist, 999, 22, 1),
                new Event(0, (int)EventTypeEnum.AttackerGoal, 999, 22, 1),
                new Event(0, (int)EventTypeEnum.DefenderGoal, 33, 23, 1),
                new Event(0, (int)EventTypeEnum.OwnGoal, 33, 23, 1),
                new Event(0, (int)EventTypeEnum.DefenderGoal, 33, 23, 1),
                new Event(0, (int)EventTypeEnum.YellowCard, 33, 23, 1),
                new Event(0, (int)EventTypeEnum.RedCard, 33, 23, 1)
            };

            Assert.Equal(15, result.Count);
            Assert.Equal(expectResult, result);
        }

        //check on error throwing / handling
        [Fact]
        public async Task UpdateEventsHandlersPlayerNotExistingCorrectly()
        {
            var points = new List<EventPoints>()
            {
                new EventPoints(1, "0-60 minutes", 1, null),
                new EventPoints(2, "60+ minutes", 1, null),
                new EventPoints(3, "Goal", 6, 1),
                new EventPoints(4, "Goal", 6, 2),
                new EventPoints(5, "Goal", 5, 3),
                new EventPoints(6, "Goal", 4, 4),
                new EventPoints(7, "Assist", 3, null),
                new EventPoints(8, "Clean sheet", 4, 1),
                new EventPoints(9, "Clean sheet", 4, 2),
                new EventPoints(10, "Clean sheet", 1, 3),
                new EventPoints(11, "Saves", 1, 1),
                new EventPoints(12, "Penalty Save", 5, 1),
                new EventPoints(13, "Penalty Miss", -2, null),
                new EventPoints(14, "Conceded", -1, null),
                new EventPoints(16, "Yellow card", -1, null),
                new EventPoints(17, "Red Card", -3, null),
                new EventPoints(18, "Own goal", -2, null),
                new EventPoints(19, "Penalty Won", 3, null),
            };

            var stubFixtureResponse = new FixtureResponse()
            {
                Fixtures = new List<Fixture>()
                {
                    new Fixture()
                    {
                        Information = new FixtureInformation() { Id = 1 },
                        Teams = new Teams()
                        {
                            Home = new Home() { Id = 22 },
                            Away = new Away() { Id = 23 },
                        },
                        Events = new List<ApiEvent>()
                        {
                            new ApiEvent() { Type = "Goal", Detail = "Normal Goal", Player = new EventPlayer { Id = 666 }, Assist = new EventPlayer { Id = 999, Name = "dave" }, Team = new ApiTeam() { Id = 22 }  },
                            new ApiEvent() { Type = "Goal", Detail = "Normal Goal", Player = new EventPlayer { Id = 666 },  Team = new ApiTeam() { Id = 22 }  }

                        },
                        Players = new List<PlayerResponse>()
                        {
                            new PlayerResponse()    //home
                            {
                                Players = new List<ApiPlayer>()
                                {
                                    new ApiPlayer()
                                    {
                                        Details = new PlayerDetails() { Id = 666 },
                                        Statistics = new List<Statistic>()
                                        {
                                            new Statistic()
                                        }
                                    },
                                    new ApiPlayer()
                                    {
                                        Details = new PlayerDetails() { Id = 999, name = "dave"},
                                        Statistics = new List<Statistic>()
                                        {
                                            new Statistic()
                                        }
                                    }
                                }
                            },
                            new PlayerResponse()   //away
                            {
                                Players = new List<ApiPlayer>()
                            }
                        }
                    }
                }
            };

            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();
            var mockPlayerRepo = this.autoMocker.GetMock<IPlayerRepository>();
            var mockEventRepo = this.autoMocker.GetMock<IEventRepository>();
            var mockMissingPlayerRepo = this.autoMocker.GetMock<IMissingPlayerRepository>();

            var result = new List<Event>();

            var stubPlayers = new List<Player>()
            {
                new Player(666, "John", "Tester", 1, null, "John Tester", 666)
            };

            mockPlayerRepo
                .Setup(x => x.Get(2000, 1, null, null))
                .ReturnsAsync(stubPlayers);
            mockEventRepo
                .Setup(x => x.Insert(It.IsAny<List<Event>>()))
                .Callback<List<Event>>(events => { result = events; });
            mockEventRepo
                .Setup(x => x.GetEventPoints())
                .ReturnsAsync(points);
            mockUnitOfWork
                .Setup(x => x.PlayerRepository)
                .Returns(mockPlayerRepo.Object);
            mockUnitOfWork
                .Setup(x => x.EventRepository)
                .Returns(mockEventRepo.Object);
            mockUnitOfWork
                .Setup(x => x.MissingPlayerRepository)
                .Returns(mockMissingPlayerRepo.Object);

            var sut = this.autoMocker.CreateInstance<EventUpdater>();

            await sut.UpdateEvents(stubFixtureResponse, mockUnitOfWork.Object);

            mockUnitOfWork
                .Verify(x => x.EventRepository.DeleteByGameId(1), Times.Once);

            mockUnitOfWork
               .Verify(x => x.MissingPlayerRepository.Insert(new MissingPlayer("dave", 22, 999)), Times.Exactly(2));

            var expectResult = new List<Event>()
            {
                new Event(0, (int)EventTypeEnum.GoalkeeperGoal, 666, 22, 1),
                new Event(0, (int)EventTypeEnum.GoalkeeperGoal, 666, 22, 1),
            };

            Assert.Equal(2, result.Count);
            Assert.Equal(expectResult, result);

        }
    }
}
