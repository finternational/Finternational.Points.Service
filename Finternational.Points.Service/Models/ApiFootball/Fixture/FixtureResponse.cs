﻿using Finternational.Points.Service.Models.ApiFootball.Common;
using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Fixture
{
    public class FixtureResponse
    {
        [JsonPropertyName("parameters")]
        public Parameters Parameters { get; set; }
        [JsonPropertyName("errors")]
        public object Errors { get; set; }
        [JsonPropertyName("results")]
        public int Results { get; set; }
        [JsonPropertyName("paging")]
        public Paging Paging { get; set; }

        [JsonPropertyName("response")]
        public List<Fixture> Fixtures { get; set; }
    }
}
