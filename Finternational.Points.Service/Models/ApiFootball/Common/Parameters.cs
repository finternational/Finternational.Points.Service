﻿using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Common
{
    public class Parameters
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
    }
}
