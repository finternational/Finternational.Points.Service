﻿using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Fixture
{
    public class PlayerGame
    {
        [JsonPropertyName("minutes")]
        public int? Minutes { get; set; }
        [JsonPropertyName("number")]
        public int? Number { get; set; }
        [JsonPropertyName("position")]
        public string Position { get; set; }
        [JsonPropertyName("rating")]
        public string Rating { get; set; }
        [JsonPropertyName("captain")]
        public bool? Captain { get; set; }
        [JsonPropertyName("substitute")]
        public bool? Substitute { get; set; }
    }
}
