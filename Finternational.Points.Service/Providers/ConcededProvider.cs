﻿using Finternational.Points.Service.Models.ApiFootball.Fixture;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Providers
{
    public class ConcededProvider : IConcededProvider
    {
        public async Task<int> Get(int playerExternalId, FixtureResponse fixture, Game game, int playerMinutes, IUnitOfWork unitOfWork)
        {
            //var assist is subbed on, check subs page
            var conceded = 0;

            if(playerMinutes == 0)
            {
                return conceded;
            }
            var player = await unitOfWork.PlayerRepository.GetByExternalId(playerExternalId);

            for (int i = 0; i < (fixture.Fixtures[0]?.Events?.Count ?? 0); i++)
            {
                var gameEvent = fixture.Fixtures[0].Events[i];
                if(gameEvent.Team.Id != player.Country.Id && gameEvent.Type == "Goal" && gameEvent?.Comments != "Penalty Shootout"
                    && gameEvent.Detail != "Missed Penalty")
                {
                    bool onPitch = true;

                    for(int j = 0; j < i; j++)
                    {
                        if (!game.TransfersReversed)
                        {
                            if (fixture.Fixtures[0].Events[j].Type == "subst" && fixture.Fixtures[0].Events[j].Player.Id == playerExternalId)
                            {
                                onPitch = false;
                                break;
                            }
                        } 
                        else
                        {
                            if (fixture.Fixtures[0].Events[j].Type == "subst" && fixture.Fixtures[0].Events[j].Assist.Id == playerExternalId)
                            {
                                onPitch = false;
                                break;
                            }
                        }
                        
                    }

                    //get all events after goal, if one of them was the player being subbed on assist == id, then not on pitch
                    for(int j = i; j < (fixture.Fixtures[0]?.Events?.Count ?? 0); j++)
                    {
                        if (!game.TransfersReversed)
                        {
                            if (fixture.Fixtures[0].Events[j].Type == "subst" && fixture.Fixtures[0].Events[j].Assist.Id == playerExternalId)
                            {
                                onPitch = false;
                                break;
                            }
                        } 
                        else
                        {
                            if (fixture.Fixtures[0].Events[j].Type == "subst" && fixture.Fixtures[0].Events[j].Player.Id == playerExternalId)
                            {
                                onPitch = false;
                                break;
                            }
                        }
                    }

                    if (onPitch)
                    {
                        conceded++;
                    }
                }
            }    

            return conceded;
        }
    }
}
