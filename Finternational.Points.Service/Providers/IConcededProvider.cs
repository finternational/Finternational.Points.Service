﻿using Finternational.Points.Service.Models.ApiFootball.Fixture;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Providers
{
    public interface IConcededProvider
    {
        Task<int> Get(int playerId, FixtureResponse fixture, Game game, int minutes, IUnitOfWork unitOfWork);
    }
}
