using Finternational.Points.Service.Configuration;
using Finternational.Points.Service.Handlers;
using Finternational.Points.Service.Providers;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service
{
    public class PointService : BackgroundService
    {
        private readonly ILogger<PointService> logger;
        private readonly IUnitOfWorkProvider unitOfWorkProvider;
        private readonly PersistenceSettings persistenceSettings;
        private readonly IFixtureHandler fixtureHandler;
        private readonly IGamesToCheckProvider gamesToCheckProvider;
        private readonly IFixturesProvider fixturesProvider;
        private readonly IDelayProvider delayProvider;
        public PointService(
            ILogger<PointService> logger, 
            IUnitOfWorkProvider unitOfWorkProvider,
            PersistenceSettings persistenceSettings,
            IFixtureHandler fixtureHandler,
            IGamesToCheckProvider gamesToCheckProvider,
            IFixturesProvider fixturesProvider,
            IDelayProvider delayProvider)
        {
            this.logger = logger;
            this.unitOfWorkProvider = unitOfWorkProvider;
            this.persistenceSettings = persistenceSettings;
            this.fixtureHandler = fixtureHandler;
            this.gamesToCheckProvider = gamesToCheckProvider;
            this.fixturesProvider = fixturesProvider;
            this.delayProvider = delayProvider;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await this.Execute(stoppingToken);
            }
        }

        public async Task Execute(CancellationToken stoppingToken)
        {       
            using (var unitOfWork = this.unitOfWorkProvider.CreateWithoutOpenTransaction(persistenceSettings.ConnectionString))
            {
                logger.LogInformation("Getting games to check");
                var games = await this.gamesToCheckProvider.Get(unitOfWork);
                logger.LogInformation($"Found {games.Count} games to check");

                logger.LogInformation("Getting fixtures to check");
                var fixtures = await this.fixturesProvider.Get(games, unitOfWork);
                logger.LogInformation($"Found {fixtures.Count} fixtures to check");

                foreach (var fixture in fixtures)
                {
                    logger.LogInformation($"Handling fixture {fixture.Parameters.Id}");
                    await this.fixtureHandler.Handle(fixture, games, unitOfWork);
                };

                try
                {
                    if (fixtures.Any())
                    {
                        logger.LogInformation($"Committing changes");
                        await unitOfWork.Commit();
                    }
                }
                catch (Exception ex)
                {
                    this.logger.LogError(ex.Message);
                    logger.LogInformation($"Sleeping for 60 minutes");
                    await this.delayProvider.DelayMinutes(stoppingToken, 60);
                }
                finally
                {
                    logger.LogInformation($"Sleeping for 5 minutes");
                    await this.delayProvider.DelayMinutes(stoppingToken, 5);
                }
            }    
        }
    }
}