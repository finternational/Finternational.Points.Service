﻿using Finternational.Points.Service.Exceptions;
using Finternational.Points.Service.Getter;
using Finternational.Points.Service.Models.ApiFootball.Fixture;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;

namespace Finternational.Points.Service.Providers
{
    public class FixturesProvider : IFixturesProvider
    {
        private readonly ILogger<IFixturesProvider> logger;
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly IFixtureGetter fixtureGetter;

        public FixturesProvider(ILogger<IFixturesProvider> logger, IDateTimeProvider dateTimeProvider, IFixtureGetter fixtureGetter)
        {
            this.logger = logger;
            this.dateTimeProvider = dateTimeProvider;
            this.fixtureGetter = fixtureGetter;
        }

        public async Task<List<FixtureResponse>> Get(List<Game> games, IUnitOfWork unitOfWork)
        {
            var fixtures = new List<FixtureResponse>();

            foreach (var game in games)
            {
                try
                {
                    fixtures.Add(await this.fixtureGetter.Get(game.Id));
                }
                catch (ApiCallException ex)
                {
                    this.logger.LogError($"Failed to get data for game {game.Id}, status code {ex.StatusCode}, error: {ex.Information}");

                    await unitOfWork.ApiCallFailRepository.Insert(
                        new ApiCallFail()
                        {
                            Information = ex.Information,
                            Url = ex.Url,
                            StatusCode = ex.StatusCode
                        });

                    if (game.Finished)
                    {
                        await unitOfWork.GameRepository.Update(game with { NextCheck = this.dateTimeProvider.Now().AddHours(12) });
                    }
                }
            }

            return fixtures;
        }
    }
}
