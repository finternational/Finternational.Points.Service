﻿using System.Text.Json.Serialization;

namespace Finternational.Points.Service.Models.ApiFootball.Fixture
{
    public class Player
    {
        [JsonPropertyName("player")]
        public PlayerDetails? Details { get; set; }

        [JsonPropertyName("statistics")]
        public List<Statistic> Statistics { get; set; }
    }
}
