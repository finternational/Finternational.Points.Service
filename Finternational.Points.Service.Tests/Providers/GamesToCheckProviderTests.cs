﻿using Finternational.Points.Service.Providers;
using finternational_common.Entities;
using finternational_common.Persistence.Interfaces;
using finternational_common.Persistence.Repositories.Interfaces;
using Moq;
using Moq.AutoMock;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Finternational.Points.Service.Tests.Providers
{
    public class GamesToCheckProviderTests
    {
        private readonly AutoMocker autoMocker = new AutoMocker();

        [Fact]
        public async Task GetReturnsCorrectResults()
        {
            var stubGames = new List<Game>()
            {
                new Game(1, 2, "team", 3, 3, "Team", 0, new DateTime(2022, 8, 09), false, null, null, null, false),  //after kickoff
                new Game(2, 2, "team", 3, 3, "Team", 0, new DateTime(2022, 8, 11), false, null, null, null, false), //before kickoff
                new Game(3, 2, "team", 3, 3, "Team", 0, new DateTime(2022, 8, 09), true, null, null, null, false), //finished
                new Game(4, 2, "team", 3, 3, "Team", 0, new DateTime(2022, 8, 11), false, new DateTime(2022, 12, 12), null, null, false) //next check
            };

            this.autoMocker.GetMock<IDateTimeProvider>()
                .Setup(x => x.Now())
                .Returns(new DateTime(2022, 8, 10));

            var mockRepo = this.autoMocker.GetMock<IGameRepository>();
            var mockUnitOfWork = this.autoMocker.GetMock<IUnitOfWork>();

            mockRepo
                .Setup(x => x.Get())
                .ReturnsAsync(stubGames);
            mockUnitOfWork
                .Setup(x => x.GameRepository)
                .Returns(mockRepo.Object);

            var sut = this.autoMocker.CreateInstance<GamesToCheckProvider>();

            var result = await sut.Get(mockUnitOfWork.Object);

            Assert.Single(result);
            Assert.Equal(1, result[0].Id);
        }
    }
}
