﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finternational.Points.Service.Providers
{
    public class DelayProvider : IDelayProvider
    {
        public async Task DelayMinutes(CancellationToken stoppingToken, int minutes)
        {
            await Task.Delay(minutes * 60000, stoppingToken);
        }
    }
}
