﻿using Finternational.Points.Service.Getter;

namespace Finternational.Points.Service.Configuration
{
    public static class GetterConfigurationExtensions
    {
        public static void AddGetters(this IServiceCollection services)
        {
            services.AddTransient<IFixtureGetter, FixtureGetter>();
        }
    }
}

